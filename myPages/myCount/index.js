// pages/my/myCount/index.js
const app = getApp()
const api = require("@/utils/url")
const util = require("@/utils/util")

Page({
  /**
   * 页面的初始数据
   */
  data: {
    baseUrl: app.globalData.hostIp,
    isFromRegister: false,
    showNickName: false,
    showImageBox: false,
    showUserName: false,
    headerImage: "",
    sexImage: '/images/male.png',
    name: '',
    nickName: '',
    sex: null,
    birthday: null,
    phoneNumber: '',
    selectSex: '',
    // 0是女，1是男
    sexList: [{
        name: '男',
        value: 1
      },
      {
        name: '女',
        value: 0
      },
    ],
    index: '', //1是性别 2是生日 3是手机4是车牌号5是昵称
    popTitle: '',
    popLabel: '',
    popVal: '',
    carCard: '',
    fileList: [
      // {
      //   //url: 'https://img.yzcdn.cn/vant/leaf.jpg',
      //   url: 'https://www.nuoyin.cc:8081/img/header/client/451.jpeg',
      //   status: 'success',
      //   message: '上传中',
      // }
    ],
    currentDate: null,
    minDate: new Date(1950, 1, 1),
    formatter(type, value) {
      if (type === 'year') {
        return `${value}年`;
      }
      if (type === 'month') {
        return `${value}月`;
      }
      return value;
    },
    dataPopShow: false,
    region: [],
    area:''
  },

  submit() {
    // if (this.data.name == '') {
    //     Toast.fail('姓名不可为空');
    //     return
    // }
    // if (this.data.nickName == '') {
    //     Toast.fail('昵称不可为空');
    //     return
    // }
    this.setData({
      showUserName: false
    })
    let Data = {
      id: this.data.clientId,
      headerImage: this.data.headerImage,
      name: this.data.name,
      nickName: this.data.nickName,
      phoneNumber: this.data.phoneNumber,
      carCard: this.data.carCard,
      sex: this.data.sex,
      birthday: this.data.birthday,
      area:this.data.region?.join(",") ||''
    }
    switch (this.data.index) {
      case '3':
        //手机号
        console.log(this.data.popVal);
        Data.phoneNumber = this.data.popVal;
        break;
      case '4':
        Data.carCard = this.data.popVal;
        break;
      case '5':
        console.log(this.data.popVal);
        Data.nickName = this.data.popVal;
        break;
    }
    api.updateClientInfo(Data, ).then((res) => {
      if (res.code == 1) {
        console.log(res,'res')
        this.setData({
          showNickName: false,
          showImageBox: false,
          showUserName: false,
          name: res.data.name,
          nickName: res.data.nickName,
          headerImage: res.data.headerImage,
          sex: res.data.sex,
          sexImage: util.getImageTobase64rUrl(res.data.sex === 1 ? '/images/male.png' : '/images/female.png'),
          phoneNumber: res.data.phoneNumber,
          birthday: res.data.birthday,
          carCard: res.data.carCard,
        })
        app.globalData.userInfo = res.data;
        wx.setStorageSync("userInfo", res.data);
        console.log(res.data)
        wx.showToast({
          title: '修改成功',
          icon: "none"
        })
      } else {
        wx.showToast({
          title: '修改失败',
          icon: "none"
        })
      }
    })
  },
  change(e) {
    this.setData({
      index: e.currentTarget.dataset.index
    })
    //1是性别 2是生日 3是手机
    switch (e.currentTarget.dataset.index) {
      case '1':
        break;
      case '2':
        const isIos = util.isIos();
        let date = new Date().getTime()
        if (this.data.birthday !== null) {
          let dateString = this.data.birthday;
          if (isIos) {
            dateString = this.data.birthday.replaceAll("-", "/")
          }
          date = new Date(dateString).getTime();
        }
        this.setData({
          dataPopShow: true,
          currentDate: date
        })
        break;
      case '3':

        this.setData({
          showNickName: true,
          popTitle: '编辑手机号',
          popLabel: '手机号',
          popVal: this.data.phoneNumber
        })
        break;
      case '4':

        this.setData({
          showNickName: true,
          popTitle: '车牌号',
          popLabel: '车牌号',
          popVal: this.data.carCard
        })
        break;
      default:

        break;

    }

  },
  bindDateChange(e) {
    this.setData({
      birthday: e.detail.value
    });
    this.submit()

  },
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value);
    const data =e.detail.value
    this.setData({
      region: e.detail.value,
      area: data.join("/")
    });
    this.submit();
  },
  //转换时间
  dateToTime(data) {
    let dates = new Date(data);
    let Y = dates.getFullYear() + '-'
    let M = (dates.getMonth() + 1 < 10 ? '0' + (dates.getMonth() + 1) : dates.getMonth() + 1) + '-';
    let D = (dates.getDate() < 10 ? '0' + dates.getDate() : dates.getDate()) + ' ';
    return Y + M + D;
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let region = app.globalData.userInfo.area;
    if(region){
      region =region?.split(",");
    }else{
      region=[]
    }
    this.setData({
      clientId: app.globalData.userInfo.id,
      name: app.globalData.userInfo.name,
      nickName: app.globalData.userInfo?.nickName,
      sex: app.globalData.userInfo.sex,
      headerImage:app.globalData.userInfo?.headerImage,
      sexImage: util.getImageTobase64rUrl(app.globalData.userInfo.sex === 1 ? '/images/male.png' : '/images/female.png'),
      phoneNumber: app.globalData.userInfo.phoneNumber,
      birthday: app.globalData.userInfo.birthday || '',
      carCard: app.globalData.userInfo.carCard,
      isFromRegister: options.from === 'resgister',
      region: region,
      area:region?.join("/") 
    })
  },
  afterRead(event) {
    let that = this
    const {
      file
    } = event.detail;
    // 当设置 mutiple 为 true 时, file 为数组格式，否则为对象格式
    this.setData({
      fileList: [{
        url: null,
        //url: 'https://www.nuoyin.cc:8081/img/header/client/23123-1698209541219.jpg',
        status: 'uploading',
        message: '上传中',
      }]
    })
    wx.uploadFile({
      url: app.globalData.hostIp + '/weixin/HeaderUpload',
      filePath: file.url,
      name: 'file',
      formData: {
        clientId: this.data.clientId
      },
      success(res) {
        var obj = JSON.parse(res.data);
        //上传完成需要更新 fileList
        if (obj.code == 1) {
         
        } else {
          if (obj.msg != null) {
            wx.showToast({
              title: obj.msg,
              icon: "none"
            })
          } else {
            wx.showToast({
              title: '上传失败',
              icon: "none"
            })
          }
        }
      },
    });
  },
  oversize() {
    // wx.showToast({
    //   title: '图片大小最大为1MB',
    //   icon:'error'
    // })
    Toast.fail('图片大小最大为1MB');
  },
  deleteImage(e) {
    console.log(e.detail.index)
    this.setData({
      fileList: {}
    })
  },
  ShowNickName() {
    this.setData({
      showNickName: true,
      popTitle: '编辑昵称',
      popLabel: '昵称',
      popVal: this.data.nickName,
      index: '5'
    })
  },
  CloseImageBox() {
    this.setData({
      showImageBox: false
    })
  },
  //打开上传头像弹框
  ShowImageBox() {
    this.setData({
      showImageBox: true
    })
  },

  CloseNickName() {
    this.setData({
      showNickName: false
    })
  },
  ShowUserName() {
    this.setData({
      showUserName: true,

    })
  },

  CloseUserName() {
    this.setData({
      showUserName: false
    })
  },
  chageSex(e) {
    this.setData({
      sex: this.data.sex == 1 ? 0 : 1,
      sexImage: util.getImageTobase64rUrl(this.data.sex === 1 ? '/images/male.png' : '/images/female.png'),
    });
    this.submit();
  },


  skipPage() {
    wx.switchTab({
      url: "/pages/index/index",
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})