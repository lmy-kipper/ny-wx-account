// pages/my/index.js
const app = getApp()
const util = require("../../utils/util.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showCode: false,
    userCodeImage: '',
    loading: false,
    titleType: '',
    sex: 0,
    sexImage: '/images/male.png',
    menuInfo: {
      top: 0
    }
  },
  onOpen() {
    this.setData({
      showCode: true
    });
  },

  onClose() {
    this.setData({
      showCode: false
    });
  },
  goGift() {
    wx.showToast({
      title: '暂未开发，敬请期待',
      icon: "none"
    })
  },
  goToDetail(e) {
    console.log(e, 'e')
    const url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      clientId: app.globalData.userInfo?.id,
      nickName: app.globalData.userInfo?.nickName,
      userCodeImage: util.getImageTobase64rUrl(app.globalData?.userCodeImage),
      sex: app.globalData.userInfo.sex,
      sexImage: util.getImageTobase64rUrl(app.globalData.userInfo?.sex === 1 ? '/images/male.png' : '/images/female.png'),
      titleType: this.getSystemInfo()
    });

    util.getSystemInfoUtil().then((res) => {
      console.log(res, 'res')
      this.setData({
        menuInfo: res,
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },
  getSystemInfo() {
    let systemInfo = wx.getSystemInfoSync()
    let platform = systemInfo.platform
    if (platform === 'android') {
      return 'android'
    } else if (platform === 'ios') {
      return 'ios'
    } else {
      return 'android'
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      clientId: app.globalData.userInfo.id,
      name: app.globalData.userInfo.name,
      sex: app.globalData.userInfo.sex,
      sexImage: util.getImageTobase64rUrl(app.globalData.userInfo.sex === 1 ? '/images/male.png' : '/images/female.png'),
      nickName: app.globalData.userInfo?.nickName,
    });
    setTimeout(() => {
      this.setData({
        loading: false
      })
    }, 300);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})