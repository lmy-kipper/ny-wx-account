const permisson = require("./permisson.js");
const userCameraName = "scope.camera"; // 摄像头权限
const userCameraZhName = "手机摄像头"; // 摄像头权限对应的中文名称
const jsQR = require("./jsQR");
const utils = require("@/utils/util")
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */


  data: {
    on: true,
    // iconScanBgGif,scancodeOpen,scancodeClose,
    canScan: false, // 是否显示自定义扫码界面
    flashBtn: 'off', // off关闭   on  打开手电筒
    scanResult: '', // 扫描结果
    isAuthRusult: true,
    tempFilePaths: '',
    showCode: false,
    userCodeImage:utils.getImageTobase64rUrl(app.globalData.userCodeImage), 
    nickName: app.globalData.userInfo.nickName,
    menuInfo: {
      top: 0
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    utils.getSystemInfoUtil().then((res) => {
      console.log(res, 'res')
      this.setData({
        menuInfo: res
      })

    })
    // wx.scanCode({
    //     success (res) {
    //         var url = res.result
    //         console.log(url)
    //         var regex1 = /^https:\/\/www\.nuoyin\.cc:8081\/img\/store\/[0-9]+\.png$/;
    //         if(regex1.test(url)){
    //           // 是商家的二维码
    //           var regex2 = /^https:\/\/www\.nuoyin\.cc:8081\/img\/store\/([0-9]+)\.png$/;
    //           var match = url.match(regex2);
    //           if(match){
    //             var storeId = match[1];
    //             console.log(storeId); // 输出 35

    //             wx.reLaunch({
    //               url: '/pages/makeCard/makeCard?storeId=' + storeId
    //             })
    //           }
    //         }
    //         else{
    //           wx.showToast({
    //             title: '无效二维码',
    //             icon: 'error',
    //             duration: 2000
    //           })
    //         }
    //     },
    //     fail(res){
    //       wx.showToast({
    //         title: '无效二维码',
    //         icon: 'error',
    //         duration: 2000
    //       })
    //     }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.scanShowClick()
  },
  onCodeOpen() {
    this.setData({
      showCode: true
    });
  },

  onCodeClose() {
    this.setData({
      showCode: false
    });
  },
  // 打开关闭手电筒
  changeflashBtn() {
    this.setData({
      flashBtn: this.data.flashBtn == 'off' ? 'on' : 'off'
    })
  },
  // 显示扫码界面(扫一扫)
  scanShowClick() {
    // 校验权限, 必须开启摄像头权限
    wx.getSetting({
      success: async (res) => {
        if (!res.authSetting['scope.camera']) {
          // 权限封装
          permisson.permission_request(userCameraName, userCameraZhName, () => {
            this.setData({
              canScan: true
            })
          });
          // 也可自己写,通过 wx.authorize 打开有关授权=>官方链接https://developers.weixin.qq.com/miniprogram/dev/api/open-api/authorize/wx.authorize.html
        } else {
          this.setData({
            canScan: true
          })
          // 顶部标题栏背景变黑
          // wx.setNavigationBarColor({
          //   backgroundColor: '#000000',
          //   frontColor: '#ffffff',
          // });
        }
      }
    })
  },
  // // 隐藏扫码界面
  // scanHideClick(){
  //   this.setData({canScan:false})
  //   // 顶部标题栏背景变白
  //   wx.setNavigationBarColor({
  //       backgroundColor: '#ffffff',
  //       frontColor: '#000000',
  //   });
  // },
  // 扫一扫返回数据
  scancode(e) {
    console.log(e.detail)
    const {
      result
    } = e.detail;
    console.log(e.detail, ' e.detail')
    if (result) {
      const storeId = utils.handleCodeInfo(result);
      console.log(storeId,'storeId')
      if (storeId) {
        wx.navigateTo({
          url: '/card/cardList/index?type=makeCard&storeId=' + storeId
        });
        return;
      }
      const cardClientId = utils.handleWeixinCodeInfo(result);
      console.log(cardClientId,'cardClientId')
      if(cardClientId){
        wx.navigateTo({
          url: '/card/bindCard/bindCard?cardClientId=' + cardClientId
        })
      }
    }
  },

  selectImage() {
    console.log("clic selectImage")
    wx.chooseImage({
      sizeType: ['compressed'],
      sourceType: ["album"],
      success: (res) => {
        const tempFilePaths = res.tempFilePaths;
        wx.showLoading({
          title: "识别中",
        });
        console.log(tempFilePaths, 'tempFilePaths')
        this.setData({
          tempFilePaths: tempFilePaths[0]
        });
        if (this.data.tempFilePaths) {
          this.getImageData()
        }
      },
      fail: (err) => {
        console.log(err);
        wx.hideLoading();
      }
    })
  },
  getImageData() {
    const ctx = wx.createCanvasContext('codeCanvas');
    // 获取图片尺寸
    wx.getImageInfo({
      src: this.data.tempFilePaths,
      success: (res) => {
        console.log(res, 'res.width')
        // 绘制
        ctx.drawImage(res.path, 0, 0, res.width, res.height);
        ctx.draw(false, () => {
          wx.canvasGetImageData({
            canvasId: 'codeCanvas',
            x: 0,
            y: 0,
            width: res.width,
            height: res.height,
            success: (img) => {
              const code = jsQR(img.data, img.width, img.height);
              if (code) {
                const data = code.data;
                wx.hideLoading();
                if (data) {
                  const storeId = utils.handleCodeInfo(data);
                  if (storeId) {
                    wx.navigateTo({
                      url: '/card/cardList/index?type=makeCard&storeId=' + storeId
                    });
                    return;
                  }
                  const cardClientId = utils.handleWeixinCodeInfo(data);
                  if(cardClientId){
                    wx.navigateTo({
                      url: '/card/bindCard/bindCard?cardClientId=' + cardClientId
                    })
                  }
                }
              } else {
                wx.hideLoading();
                wx.showToast({
                  title: '图片无法识别，请重新选择图片',
                  icon: "error"
                })
              }
            }
          }, this)
        });
      },
      fail: () => {
        wx.hideLoading();
      }
    });
  },
  backCard() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  initCameraDone(){

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    var that = this
    that.setData({
      flashBtn: 'off',
      canScan: false
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})