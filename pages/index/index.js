const api = require("@/utils/url")
const app = getApp()
const util = require("@/utils/util")
Page({
    data: {
        cardClientList: [],
        loading: true
    },
    //进入详情页
    toCardClientDetail(e) {
        var cardClientId = e.currentTarget.dataset.item
        wx.navigateTo({
            //   url: '/card/cardList/index?id=' + cardClientId,
            url: '/card/cardDetail/index?id=' + cardClientId,
        })
        return
    },
    getList(){

        var that = this
        if (app.globalData.userInfo?.id) {
            api.getIndexList({
                clientId: app.globalData.userInfo?.id
            }).then((res) => {
                that.setData({
                    cardClientList: res.data,
                    loading: false
                })


            })
        } else {
            that.setData({
                loading: false
            })
        }

    },
    onLoad(options) {
        var that = this
        that.getList()
    },
    onPullDownRefresh() {
        var that = this
        if (app.globalData.userInfo?.id) {
            api.getIndexList({
                clientId: app.globalData.userInfo?.id
            }).then((res) => {
                that.setData({
                    cardClientList: res.data,
                    loading: false
                })
                wx.stopPullDownRefresh()
            })
        } else {
            that.setData({
                loading: false
            })
        }
    }

})