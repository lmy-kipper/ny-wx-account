// pages/register/index.js
const api = require("@/utils/url");
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isCheck: false,
    showPrivacy: false,
    phone: null,
    openId: null,
    unionId: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // wx.getPrivacySetting({
    //   success: res => {
    //     console.log(res) // 返回结果为: res = { needAuthorization: true/false, privacyContractName: '《xxx隐私保护指引》' }
    //     if (res.needAuthorization) {
    //       // 需要弹出隐私协议
    //       this.setData({
    //         showPrivacy: true
    //       })
    //     } else {
    //       // 用户已经同意过隐私协议，所以不需要再弹出隐私协议，也能调用已声明过的隐私接口
    //       // wx.getUserProfile()
    //       // wx.chooseMedia()
    //       // wx.getClipboardData()
    //       // wx.startRecord()
    //     }
    //   },
    //   fail: () => {},
    //   complete: () => {}
    // });
  },
  chageCheck() {
    this.setData({
      isCheck: !this.data.isCheck
    })
  },
  login(){
    if(!this.data.isCheck){
      wx.showToast({
        title: '请选勾选用户协议和隐私政策',
        icon:"none"
      })
      return 
    }
  },
  bindgetphonenumber(e) {
    try {
      console.log(e, 'ee');
      wx.showLoading();
      wx.login({
        fail:()=>{
          wx.hideLoading()();
        },
        success: (res) => {
          if (res.code) {
            // 获取openid
            api.getOpenId({
              code: res.code
            }).then((res) => {
              this.setData({
                openId: res.data
              });
              // 登录
              api.loginApi({
                openId: this.data.openId
              }).then((res) => {
                // 未注册
                if (res.code === 0) {
                  // 获取手机
                  api.getPhoneNumber({
                    code: e.detail.code
                  }).then((res) => {
                    this.setData({
                      phone: res.data.replaceAll('"', '')
                    })

                    api.getIds({
                        code: e.detail.code
                      }).then((res) => {
                        this.setData({
                            unionId: res.data.unionId
                        })


                    // 注册
                    api.registerApi({
                        openId: this.data.openId,
                        phoneNumber: this.data.phone,
                        unionId: this.data.unionId
                      }).then((res) => {
                        app.globalData.userInfo = res.data;
                        wx.setStorageSync("userInfo", res.data);
                        wx.hideLoading();
                        wx.showToast({
                          title: '注册成功',
                        });
                        wx.redirectTo({
                          url: "/myPages/myCount/index?from=resgister",
                        });
                      });
                    })


                  }).catch(() => {
                    wx.hideLoading();
                  });
                } else {
                  // 设置数据
                  app.globalData.userInfo = res.data;
                  wx.setStorageSync("userInfo", res.data);
                  wx.hideLoading();
                  wx.showToast({
                    title: '登录成功',
                  })
                  wx.switchTab({
                    url: "/pages/index/index",
                  });
                }
              }).catch(() => {
                wx.hideLoading();

              });;
            })
          } else {
            console.log('登录失败！' + res.errMsg)
            wx.hideLoading();
          }
        }
      })








    } catch (e) {
      console.log(e, 'ee')
    }




  },
  goToAgreement() {
    wx.navigateTo({
      url: '/myPages/agreement/index?type=regisiter_agreement',
    })
  },
  goToPrivacyAgreement() {
    wx.navigateTo({
      url: '/myPages/agreement/index?type=private_agreement',
    })
  },
  handleAgreePrivacyAuthorization() {
    // 用户同意隐私协议事件回调
    // 用户点击了同意，之后所有已声明过的隐私接口和组件都可以调用了
    // wx.getUserProfile()
    // wx.chooseMedia()
    // wx.getClipboardData()
    // wx.startRecord()
  },
  handleOpenPrivacyContract() {
    // 打开隐私协议页面
    wx.openPrivacyContract({
      success: () => {}, // 打开成功
      fail: () => {}, // 打开失败
      complete: () => {}
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})