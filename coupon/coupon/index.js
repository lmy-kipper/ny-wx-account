// pages/coupon/index.js
const app = getApp()
const api = require("@/utils/url")
Page({
    data: {
        showModal:false,
        list:[],//优惠券列表
        url:'',//优惠券二维码
        dialogTitle:'',
    },
    onLoad(options) {
        console.log(options);
        api.getCouponList({
            cardClientId: options.cardId || 1684519645807849553
        }).then((res) => {
            this.setData({
                list:res.data
            })
        }).catch((err)=>{
            console.log(err);
        })
    },
    masktap(){
        this.setData({
            showModal:false
          })
    },
    // 弹出层里面的弹窗
    ok:function () {
      this.setData({
        showModal:false
      })
    }
})