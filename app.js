// app.js
App({
  onLaunch() {
    const userInfo =wx.getStorageSync('userInfo');
    if(userInfo){
      this.globalData.userInfo = userInfo;
      this.globalData.userCodeImage = this.globalData.hostIp +'/img/client/'+userInfo?.id+'.png';
    }else{
      wx.redirectTo({
        url: "/pages/register/index",
      })
    }
  },
  globalData: {
    // userInfo: {
    //     id:46
    // },
    openid: null,
    //hostIp: 'https://127.0.0.1:8082',
    hostIp: 'https://www.nuoyin.cc:8080',
    // userCodeImage:'https://www.nuoyin.cc:8080'+
  }
})
// __usePrivacyCheck__":true