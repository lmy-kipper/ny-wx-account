Component({
  properties: {
    titleType:{
        type: String,
        value: ''
    },
    navH:{
        type: Number,
        value: 0
    },
    navTitle:{
        type: Object,
        value: {}
    },
    titleText:{
      type: String,
        value: ''
    }

  },
  data: {
  },
  methods: {
    // 返回上一页
    goback(e) {
      // 返回上一级
      if (e.currentTarget.dataset.type == 'back') {
        wx.navigateBack({
          delta: 1,
        })
      } else { // 回首页
        wx.switchTab({
          url: '/pages/index/index',
        })
      }
    },
  },
})