// myPages/official/index.js

Component({
    properties: {
        name: {
            officialAccount: Boolean,
            value: ''
        }
    },
    methods: {
        //关闭公众号组件
        closeOfficialAccount: function () {
            this.setData({
                officialAccount: true
            });
        },

        //當時用了組件才會顯示
        bindload: function () {
            this.setData({
                'closeImg': false,
            })
        },
    },
})