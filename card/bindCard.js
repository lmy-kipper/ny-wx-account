// card/bindCard.js
const app = getApp()
const api = require("@/utils/url")
const util = require("@/utils/util")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageUrl: "https://www.nuoyin.cc:8081/img/cardFace/jianshen.png",
    userInfo: {},
    loading: true,
    card: {},
    isVFocus: false,
    vCodeValue: '',
    isVFocus: true,
    cardClientId: ''

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.data.userInfo = wx.getStorageSync('userInfo');
    this.setData({
      phoneNumber: this.data.userInfo.phoneNumber,
      cardClientId: options.cardClientId ||'',
      clientId: app.globalData.userInfo.id
    })
    if(options.q){
      const url = decodeURIComponent(options.q);
      console.log(url,'url')
      const cardClientId  =util.handleWeixinCodeInfo(url);
      console.log(cardClientId)
      if(cardClientId){
        this.setData({
          cardClientId: cardClientId,
        })
      }
    }
    const cardClientId = this.data.cardClientId;
    var this_ = this
    if (cardClientId != null) {
      api.getCardDetailList({
        cardId: cardClientId
      }).then((res) => {
        this.setData({
          card: res.data,
          imageUrl: util.getImageTobase64rUrl(res.data.patternUrl),
          type: res.data.type
        })
        this.setData({
          loading: false
        })
      })
    }
  },
  showVCode: function (e) {
    const that = this;
    that.setData({
      vCodeValue: e.detail.value,
    });
    if(e.detail.value?.length>=4){
      this.realSubmit()
    }
  },
  tapFn(e) {
    const that = this;
    that.setData({
      isVFocus: true,
    });
  },
  realSubmit() {
   const phoneTail = this.data.vCodeValue.substr(0,4);
  console.log({
    phoneTail: phoneTail,
    clientId: this.data.clientId,
    cardClientId: this.data.cardClientId
  },'parms')
    api.weixinBindClient({
      phoneTail: phoneTail,
      clientId: this.data.clientId,
      cardClientId: this.data.cardClientId
    }).then((res) => {
      console.log(res,'res')
      if (res.code == 1) {
        wx.showToast({
          title: '申请办卡成功，请等待商家审核',
          duration: 3000,
          icon: "none"
        })
        const timeout = setTimeout(() => {
          clearTimeout(timeout);
          //要延时执行的代码
          wx.switchTab({
            url: '/pages/index/index',
          })
        }, 3000) //延迟时间
      } else if (res.code == 0) {
        wx.showToast({
          title: res.msg ||'绑定失败',
          icon: 'error',
          duration: 2000
        })
      } else {
        wx.showToast({
          title: '请求失败',
          icon: 'error',
          duration: 2000
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})