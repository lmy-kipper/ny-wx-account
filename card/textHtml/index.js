// pages/outHtml/Private/Private.js
const app = getApp()
const HttpReq = require('@/utils/HttpReq')
const api = require("@/utils/url")
Page({

    /**
     * 页面的初始数据
     */
    data: {
        content: ""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // let url = app.globalData.hostIp + '/weixin/getAgreement'
        // let data = {
        //     name:'about_us'
        // }
        // HttpReq.httpsRequest(url,data,'GET').then((res)=>{
        //     console.log(res);
        //     this.setData({
        //         content:res.data.content
        //     })
        // }).catch((err)=>{
        //     console.log(err);
        // })
        wx.setNavigationBarTitle({
            title: options.name
        })
        this.setData({
            content: options.content
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})