// pages/index/index.js
const HttpReq = require("../../utils/HttpReq");
const api = require("@/utils/url");
const utils =require("@/utils/util")

// 获取应用实例
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    cardClientList: [],
    getTopInfo: {},
    navH: 0,
    navTitle: '',
    // 是否办卡
    makeCard: false,
    // 店铺id
    storeId: '',
    loading:true,
    titleType: '',
    titleText:''
  },
  //进入详情页
  toMakeCardDetail(e) {
    var cardClientId = e.currentTarget.dataset.item;
    // var type = e.currentTarget.dataset.type;
    if (this.data.makeCard) {
      wx.navigateTo({
        url: '/card/makeCard/index?dealType=' + 2 + '&cardId=' + cardClientId,
        // url: '/card/cardList/index?dealType=2&storeId=' + this.data.storeId
      })
    } else {
      wx.navigateTo({
        url: '/card/cardDetail/index?id=' + cardClientId,
      })
    }
    return
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 扫码进来

    // 办卡
    if (options.type === 'makeCard' || options.q) {
      wx.setNavigationBarTitle({
        title: '办理会员卡' // 替换成你想要设置的标题
      });
      if (options.type === 'makeCard') {
        this.setData({
          makeCard: true,
          storeId: options.storeId
        })
      }
      if (options.q) {
        const url = decodeURIComponent(options.q);
        console.log(url, 'url');
        const storeId = utils.handleCodeInfo(url);
        this.setData({
          makeCard: true,
          storeId: storeId
        })
      }
      this.getShopCard()

    } else {
      wx.setNavigationBarTitle({
        title: '会员卡' // 替换成你想要设置的标题
      })
      this.getUserCard()
    }


    // 子
    const systemInfo = wx.getSystemInfoSync(); // 获取系统信息
    const menuButtonInfo = wx.getMenuButtonBoundingClientRect(); // 胶囊按钮位置信息
    const getTopInfo = {
      statusBarHeight: systemInfo.statusBarHeight, //状态栏高度
      navBarHeight: systemInfo.statusBarHeight + menuButtonInfo.height + (menuButtonInfo.top - systemInfo.statusBarHeight) * 2, // 导航栏高度
    };
    const navH = getTopInfo.navBarHeight //导航栏高度
    const navTitle = getTopInfo.navBarHeight - getTopInfo.statusBarHeight //导航栏标题高度
    this.setData({
      getTopInfo: getTopInfo,
      navH: navH,
      navTitle: navTitle,
      titleType: this.getSystemInfo()
    })

  },
  getShopCard() {
    api.queryCardByRetailId({
      retailerId: this.data.storeId
    }).then((res) => {
      if (res.code == 1) {
        this.setData({
          cardClientList: res.data,
          loading:false
        });
        if(res.data?.[0]){
          this.getStoreInfo(res.data?.[0].storeId);
        }
      }
    })
  },
  getStoreInfo(storeId) {
    api.queryStoreInfo(
      storeId
    ).then((res) => {
      if (res.code == 1) {
        this.setData({
          titleText:res.data.name
        })
      }
    })
  },
  getUserCard() {
    var that = this
    let url = app.globalData.hostIp + '/weixin/getAllCardClientByClientId?clientId=' + app.globalData.userInfo?.id
    HttpReq.httpsRequest(url, null, 'GET').then(res => {
      if (res.code == 1) {
        that.setData({
          cardClientList: res.data
        })
      }
    })
  },

  getSystemInfo() {
    let systemInfo = wx.getSystemInfoSync()
    let platform = systemInfo.platform
    if (platform === 'android') {
       return 'android'
    } else if (platform === 'ios') {
        return 'ios'
    } else {
        return 'android'
    }
},


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})