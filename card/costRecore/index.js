// pages/index/index.js
const api = require("@/utils/url")
const app = getApp()

Page({
    data: {
        cardClientList: [],
        loading:true
    },
    //进入详情页
    toMakeCardDetail(e) {
        // var cardClientId = e.currentTarget.dataset.item
        // wx.navigateTo({
        //   url: '/card/cardDetail/index?id=' + cardClientId,
        // })
        // return
    },
    onLoad(options) {
        var that = this
        api.getCostRecoreList({
            id: options.id
            // id: 1684519645807849551
        }).then((res) => {
            that.setData({
                cardClientList: res.data,
                loading:false
            })
        }).catch((err) => {
            console.log(err);
        })
    }
})