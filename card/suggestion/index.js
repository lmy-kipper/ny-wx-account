// pages/my/feedback/index.js
const app = getApp()
const api = require("@/utils/url")
import Toast from '@vant/weapp/toast/toast';
Page({
    data: {
        storeId:"",//商家id
        textVal:'',//输入框内容
        mobile:"",//手机号
    },
    onLoad(options) {
        this.setData({
            storeId: options.storeId
        })
    },
    sumbit(){
        let param = {
            clientId:app.globalData.userInfo.id,
            storeId:this.data.storeId,
            content:this.data.textVal,
            phoneNumber:this.data.mobile
        }
        api.addClientFeedBackToRetailer(param).then((res) => {
            Toast.success(res.msg)
            setTimeout(() => {
                wx.navigateBack()
            }, 1000);
        }).catch((err) => {
            Toast.fail(res.msg)
        })
    }
})