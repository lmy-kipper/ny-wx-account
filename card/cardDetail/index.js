// pages/makeCard2/makeCard2.js
const app = getApp()
const api = require("@/utils/url")
const util = require("@/utils/util")
const date = {
    '1月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
    '2月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日'],
    '3月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
    '4月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日'],
    '5月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
    '6月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日'],
    '7月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
    '8月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
    '9月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日'],
    '10月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
    '11月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日'],
    '12月': ['1日', '2日', '3日', '4日', '5日', '6日', '7日', '8日', '9日', '10日', '11日', '12日', '13日', '14日', '15日', '16日', '17日', '18日', '19日', '20日', '21日', '22日', '23日', '24日', '25日', '26日', '27日', '28日', '29日', '30日', '31日'],
};
Page({
    data: {
        url: app.globalData.hostIp,
        imageUrl: "https://www.nuoyin.cc:8081/img/cardFace/jianshen.png",
        couponNum: '',
        showUserInfo: true,
        userInfo: {},
        birthdayShow: '',
        phoneNumber: '',
        carId: '',
        cardClientId: null,
        birthday: '',
        radio: '1',
        retailerId: '',
        retailerName: '',
        input: '',
        type: 1, // 1计次卡，2计时卡，3储值卡，4积分卡
        tag: 1,
        inComputing: false,
        storeName: '',
        show: false,
        delShow: false,
        businessData: {},
        timeColumns: [{
                values: Object.keys(date),
                className: 'column1',
            },
            {
                values: date['1月'],
                className: 'column2',
                defaultIndex: 0,
            },
        ],
        card: {},
        retailerList: [],
        dealType: '', //续卡是3
        loading:true
    },
    onTimePickerChange(event) {
        const {
            picker,
            value,
            index
        } = event.detail;
        console.log(value)
        picker.setColumnValues(1, date[value[0]]);
    },
    onTimePickerConfirm(event) {
        const {
            picker,
            value,
            index
        } = event.detail;
        // 先判断月份
        var yue = ''
        if (value[0].length == 2) {
            yue = '0' + value[0].substring(0, 1)
        } else {
            yue = value[0].substring(0, 2)
        }
        var ri = ''
        if (value[1].length == 2) {
            ri = '0' + value[1].substring(0, 1)
        } else {
            ri = value[1].substring(0, 2)
        }
        var re = yue + '-' + ri
        console.log(re)
        this.setData({
            birthdayShow: value[0] + value[1],
            birthday: re,
        })
    },
    goToDetail(e) {
        const url = e.target.dataset.url;
        wx.navigateTo({
            url: url,
        })
    },
    ToCoupon(e) {
        const cardId = this.data.card.id
        wx.navigateTo({
            url: '/coupon/coupon/index?cardId=' + cardId,
        })
    },

    onInput(event) {
        this.setData({
            currentDate: event.detail,
        });
    },

    getCouponLen(couponList) {
        if (couponList == null) {
            this.setData({
                couponNum: 0
            })
            return
        } else if (couponList == '') {
            this.setData({
                couponNum: 0
            })
            return
        } else {
            var strList = couponList.split(',')
            this.setData({
                couponNum: strList.length
            })
        }
    },
    onLoad(options) {
        this.data.userInfo = wx.getStorageSync('userInfo');
        this.setData({
            phoneNumber: this.data.userInfo.phoneNumber,
            carId: this.data.userInfo.carCard,
            cardId: options.cardId,
            dealType: options.dealType,
            cardClientId: options.cardClientId
        })
        const cardId = options.id
        var this_ = this
        if (cardId != null) {
            api.getCardDetailList({
                cardId: cardId
            }).then((res) => {
                this.setData({
                    card: res.data,
                    imageUrl: util.getImageTobase64rUrl(res.data.patternUrl),
                    type: res.data.type
                })
                this.getCouponLen(res.data.couponList)
                this.setData({
                  loading:false
                })
            })
        }
    },
    onOpen() {
        this.setData({
            clientId: app.globalData.userInfo.id
        })
        // this.setData({
        //     show: true
        // });
    },
    onClose() {
        this.setData({
            show: false
        });

    },
    onDelClose() {
        this.setData({
            delShow: false
        });
    },
    getBusinessTel() {
        wx.makePhoneCall({
            phoneNumber: this.data.card.phoneNumber
        })
    },
    getCostList(e) {
        const id = e.currentTarget.dataset.id
        wx.navigateTo({
            url: '/card/costRecore/index?id=' + id,
        })
    },
    delCard() {
        this.setData({
            delShow: true
        });
    },
    delCancel() {
        this.setData({
            delShow: false
        });

    },

    delConfirm() {
        let that = this
        api.delCardDetailList({
            cardClientId: that.data.card.cardId || '',
            clientId: that.data.card.clientId || '',
        }).then((res) => {
            that.setData({
                delShow: false
            })
            wx.showToast({
                title: '删除成功！',
                icon: 'success',
                duration: 2000,
                mask: true,
                success: function () {
                    setTimeout(function () {
                        //要延时执行的代码
                        wx.reLaunch({
                            url: '/pages/index/index' //页面路径
                        })
                    }, 2000) //延迟时间
                }
            })
        }).catch(() => {
            wx.showToast({
                title: res.msg,
                duration: 2000,
                icon: 'error',
                duration: 2000
            })
        })
    },
    suggestion(e) {
        var storeId = e.currentTarget.dataset.storeid
        wx.navigateTo({
            url: '/card/suggestion/index?storeId=' + storeId
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})