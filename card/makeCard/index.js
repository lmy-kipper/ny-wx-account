// pages/makeCard/index.js
const app = getApp()
const api = require("@/utils/url")
const util = require("@/utils/util")

Page({
  data: {
    imageUrl: "https://www.nuoyin.cc:8081/img/cardFace/jianshen.png",
    couponNum: 0,
    clientName: '',
    showUserInfo: true,
    userInfo: {},
    phoneNumber: '',
    carCard: '',
    cardClientId: null,
    birthday: '',
    radio: '1',
    showPicker: false,
    retailerId: '',
    retailerName: '',
    input: '',
    type: 1, // 1计次卡，2计时卡，3储值卡，4积分卡
    tag: 1,
    inComputing: false,
    storeName: '',
    show: false,
    card: {},
    retailerList: [],
    dealType: '', //续卡是3
    storeId: '',
    showBirthdayPop: false,
    currentDate: new Date().getTime(),
    minDate: new Date(1900, 0, 0),
  },

  goToDetail(e) {
    const url = e.target.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
  onCancel() {
    this.setData({
      showPicker: false
    })
  },
  onConfirm(event) {
    const {
      picker,
      value,
      index
    } = event.detail;
    console.log(event.detail, 'event.detail')
    this.setData({
      retailerName: value.realName,
      retailerId: value.id,
      showPicker: false
    })

  },

  getRetailerList() {
    api.getStaff({
      storeId: this.data.storeId
    }).then((res) => {
      if (res.code == 1) {
        this.setData({
          retailerList: res.data,
          showPicker: true
        })
      }
    })
  },

  getCouponLen(couponList) {
    if (couponList == null) {
      this.setData({
        couponNum: 0
      })
      return
    } else if (couponList == '') {
      this.setData({
        couponNum: 0
      })
      return
    } else {
      var strList = couponList.split(',')
      this.setData({
        couponNum: strList.length
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const userInfo = app.globalData.userInfo;
    this.setData({
      // 办卡类型
      dealType: options.dealType,
      cardClientId: options.cardClientId,
      cardId: options.cardId,
      clientName: userInfo.name,
      phoneNumber: userInfo.phoneNumber,
      carCard: userInfo.carCard,
      birthday: userInfo.birthday,
      radio: app.globalData.userInfo.sex !== null ? String(app.globalData.userInfo.sex) : '1'
    })
    console.log(app.globalData.userInfo.sex !== null ? app.globalData.userInfo.sex.toString() : '1');
    const cardId = options.cardId
    if (cardId != null) {
      this.getCardInfo(cardId)
    }
  },
  getCardInfo(cardId) {
    api.queryCardTemplate(cardId).then((res) => {
      if (res.code == 1) {
        this.setData({
          card: res.data,
          imageUrl:  util.getImageTobase64rUrl(res.data?.patternUrl),
          type: res.data?.type,
          storeId: res.data?.storeId
        })
        this.getCouponLen(res.data.coupon)
        // if (this.data.dealType == 2) {
        //   // 新办卡
        // }
        this.getIsNew()
        this.getStoreInfo()
      }
    })
  },
  getIsNew() {
    api.queryIsNewInStore({
      storeId: this.data.storeId,
      phoneNumber: this.data.phoneNumber
    }).then((res) => {
      if (res.code == 1) {
        this.setData({
          showUserInfo: res.data,
        })
      }

    })
  },
  getStoreInfo() {
    api.queryStoreInfo(
      this.data.storeId
    ).then((res) => {
      if (res.code == 1) {
        this.setData({
          storeName: res.data.name,
          storeHeaderUrl: res.data.headerImage
        })
      }

    })
  },
  realSubmit() {
    if (this.data.retailerId == '') {
      wx.showToast({
        title: '要选择办理员工',
        icon: 'error',
      })
      return
    }
    if (this.data.clientName == '' && this.data.dealType == 2) {
      wx.showToast({
        title: '姓名不可为空',
        icon: 'error',
      })
      return
      //Toast('姓名不可为空')
    }
    if (this.data.phoneNumber == '' && this.data.dealType == 2) {
      wx.showToast({
        title: '手机号码不可为空',
        icon: 'error',
      })
      console.log(this.data.phoneNumber)
      console.log(this.data.retailerId)
      return
      //Toast('手机号码不可为空')
    }

    //办卡
    if (this.data.dealType == 2) {
      this.addCard();
    } else if (this.data.dealType == 3) {
      //续卡
      this.xuCard()
    }


  },
  //办卡
  addCard() {
    var Data = {
      clientId: app.globalData.userInfo.id,
      // clientName: this.data.clientName,
      retailerId: this.data.retailerId,
      dealType: Number(this.data.dealType),
      cardClientId: this.data.cardClientId,
      birthday: this.data.birthday,
      cardId: this.data.cardId,
      carCard: this.data.carCard,
      sex:Number( this.data.radio),
      phoneNumber: this.data.phoneNumber,
      isNew: this.data.showUserInfo == true ? 1 : 0
    }
    api.addConsumerReq(Data).then((res) => {
      if (res.code == 1) {
        wx.showToast({
          title: '申请办卡成功，请等待商家审核',
          duration: 3000,
          icon: "none"
        })
        const timeout = setTimeout(() => {
          clearTimeout(timeout);
          //要延时执行的代码
          wx.switchTab({
            url: '/pages/index/index',
          })
        }, 3000) //延迟时间
      } else if (res.code == 0) {
        wx.showToast({
          title: res.msg,
          icon: 'error',
          duration: 2000
        })
      } else {
        wx.showToast({
          title: '请求失败',
          icon: 'error',
          duration: 2000
        })
      }
    })
  },
  //续卡
  xuCard() {
    var Data = {
      dealType: Number(this.data.dealType),
      cardClientId: this.data.cardClientId,
      cardId: this.data.cardId,
      phoneNumber: this.data.phoneNumber,
    }
    api.addConsumerReq(Data).then((res) => {
      if (res.code == 1) {
        wx.showToast({
          title: res.msg,
          icon: 'suceess',
          duration: 3000,
          icon: "none"
        });
        const timeout = setTimeout(() => {
          clearTimeout(timeout)
          //要延时执行的代码
          wx.switchTab({
            url: '/pages/index/index',
          })
        }, 3000) //延迟时间
      } else if (res.code == 0) {
        wx.showToast({
          title: res.msg,
          icon: 'error',
          duration: 2000
        })
      } else {
        wx.showToast({
          title: '请求失败',
          icon: 'error',
          duration: 2000
        })
      }
    })
  },
  birthdayPopClose() {
    this.setData({
      showBirthdayPop: false
    })
  },
  openBirthday() {
    const isIos = util.isIos();
    let date = new Date().getTime()
    if (this.data.birthday !== null) {
      let dateString = this.data.birthday;
      if (isIos) {
        dateString = this.data.birthday.replaceAll("-", "/")
      }
      date = new Date(dateString).getTime()
    }
    this.setData({
      showBirthdayPop: true,
      currentDate: date
    })
  },
  //生日弹出确认
  birthdayConfirm(e) {
    this.setData({
      birthday: this.dateToTime(e.detail),
      showBirthdayPop: false
    })
  },
  //转换时间
  dateToTime(data) {
    let dates = new Date(data);
    let Y = dates.getFullYear() + '-'
    let M = (dates.getMonth() + 1 < 10 ? '0' + (dates.getMonth() + 1) : dates.getMonth() + 1) + '-';
    let D = (dates.getDate() < 10 ? '0' + dates.getDate() : dates.getDate()) + ' ';
    return Y + M + D;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})