const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return `${[year, month, day].map(formatNumber).join('/')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : `0${n}`
}
const getSystemInfoUtil = () => {
  return new Promise((resolve, reject) => {
    wx.getSystemInfo({
      success: (res) => {
        let menuButtonInfo = {};
        if (res.platform === "ios") {
          // ios设备的胶囊按钮都是固定的
          menuButtonInfo = {
            width: 87,
            height: 32,
            left: res.screenWidth - 7 - 87,
            right: res.screenWidth - 7,
            top: res.statusBarHeight + 4,
            bottom: res.statusBarHeight + 4 + 32,
          };
        } else {
          // 安卓通过api获取
          menuButtonInfo = wx.getMenuButtonBoundingClientRect()
        }
        console.log("获取胶囊信息：", menuButtonInfo);
        // 导航栏高度 = 状态栏到胶囊的间距（胶囊距上未知-状态栏高度）* 2 + 胶囊高度 + 状态栏高度
        menuButtonInfo.navHeight =
          (menuButtonInfo.top - res.statusBarHeight) * 2 +
          menuButtonInfo.height +
          res.statusBarHeight +
          1 * 3;
        // 按钮上下边距高度
        menuButtonInfo.menuBottom = menuButtonInfo.top - res.statusBarHeight;
        // 导航栏右边到屏幕边缘的距离
        menuButtonInfo.menuRight = res.screenWidth - menuButtonInfo.right;
        // 导航栏高度
        menuButtonInfo.menuHeight = menuButtonInfo.height + 6;
        resolve(menuButtonInfo);
      },
      fail(err) {
        reject({})
      },
    });
  })

}
const handleCodeInfo = (url) => {
  if (url) {
    var regex1 = /^https:\/\/www\.nuoyin\.cc:8081\/img\/retailer\/[0-9]+\.png$/;
    console.log(regex1.test(url));
    if (regex1.test(url)) {
      // 是商家的二维码
      var regex2 = /^https:\/\/www\.nuoyin\.cc:8081\/img\/retailer\/([0-9]+)\.png$/;
      var match = url.match(regex2);
      console.log(match);
      if (match) {
        var storeId = match[1];
        console.log(storeId); // 输出 35
        return storeId;

      }
    }
  }
  return '';
};
const handleWeixinCodeInfo = (url) => {
  if (url) {
    if ( url.indexOf('https://www.nuoyin.cc:8081/bind-card/')>=0) {
      // 是商家的二维码
      return url.replaceAll("https://www.nuoyin.cc:8081/bind-card/",'')
    }
  }
  return '';
};
// key value
const allImageData = {};
const getImageTobase64rUrl =  (path) => {
  if(allImageData[path]){
    return allImageData[path]
  }else{
    wx.request({
      url: path,
      responseType: 'arraybuffer', //关键的参数，设置返回的数据格式为arraybuffer
      success: res => {
        //把arraybuffer转成base64
        let data = wx.arrayBufferToBase64(res.data);
        const imageData ='data:image/jpeg;base64,' + data;
        allImageData[path] =imageData;
      }
    });
    return path
  }
}


// 是否是苹果
const isIos = () => {
  const info = wx.getSystemInfoSync();
  console.log(info);
  return info.platform === 'ios'
}
module.exports = {
  formatTime,
  getSystemInfoUtil,
  handleCodeInfo,
  isIos,
  getImageTobase64rUrl,
  handleWeixinCodeInfo
}