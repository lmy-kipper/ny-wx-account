const HttpReq = require('@/utils/HttpReq.js')
const app = getApp()

export const getPhoneNumber = async (data) => {
    const url = app.globalData.hostIp + '/weixin/getPhoneNumber'
    return HttpReq.httpsRequest(url, data, 'GET');
}

export const loginApi = async (data) => {
    const url = app.globalData.hostIp + '/client/login'
    return HttpReq.httpsRequest(url, data, 'GET');
}
export const getOpenId = async (data) => {
    const url = app.globalData.hostIp + '/weixin/getOpenId'
    return HttpReq.httpsRequest(url, data, 'GET');
}
export const registerApi = async (data) => {
    const url = app.globalData.hostIp + '/client/register'
    return HttpReq.httpsRequest(url, data, 'GET');
}
export const getIds = async (data) => {
    const url = app.globalData.hostIp + '/weixin/get-ids'
    return HttpReq.httpsRequest(url, data, 'GET');
}

export const headerUpload = async (data) => {
    const url = app.globalData.hostIp + '/weixin/HeaderUpload'
    return HttpReq.httpsRequest(url, data, 'POST');
}
export const updateClientInfo = async (data) => {
    const url = app.globalData.hostIp + '/weixin/updateClientInfo'
    return HttpReq.httpsRequest(url, data, 'POST');
}

export const HeaderUpload = async (data) => {
    const url = app.globalData.hostIp + '/weixin/HeaderUpload'
    return HttpReq.httpsRequest(url, data, 'POST');
}
export const getAgreement = async (data) => {
    const url = app.globalData.hostIp + '/weixin/getAgreement'
    return HttpReq.httpsRequest(url, data, 'GET');
}
export const addConsumerReq = async (data) => {
    const url = app.globalData.hostIp + '/weixin/addConsumerReq'
    return HttpReq.httpsRequest(url, data, 'POST');
}
export const queryCardByRetailId = async (data) => {
  const url = app.globalData.hostIp + '/weixin/queryCardByRetailId'
  return HttpReq.httpsRequest(url, data, 'GET');
}
export const queryCardTemplate = async (id) => {
  const url = app.globalData.hostIp + '/weixin/card/'+id;
  return HttpReq.httpsRequest(url, {}, 'GET');
}
export const queryIsNewInStore = async (data) => {
  const url = app.globalData.hostIp + '/weixin/clientStore/queryIsNew';
  return HttpReq.httpsRequest(url, data, 'GET');
}
export const queryStoreInfo = async (id) => {
  const url = app.globalData.hostIp + '/weixin/store/'+id;
  return HttpReq.httpsRequest(url, {}, 'GET');
}
export const getStaff = async (data) => {
  const url = app.globalData.hostIp + '/weixin/retailer/GetStaff';
  return HttpReq.httpsRequest(url, data, 'GET');
}

// 会员卡部分

export const getIndexList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/getAllCardClientByClientId'
    return HttpReq.httpsRequest(url, data, 'GET');
}


export const getCardDetailList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/cardClient/' + data.cardId
    return HttpReq.httpsRequest(url, '', 'GET');
}


export const delCardDetailList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/deleteCardClientByClient?cardClientId=' + data.cardClientId + '&clientId=' + data.clientId
    return HttpReq.httpsRequest(url, data, 'GET');
}

// 优惠券列表

export const getCouponList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/coupon/queryByCardClientId?cardClientId=' + data.cardClientId
    return HttpReq.httpsRequest(url, data, 'GET');
}


export const getCardList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/getAllCardClientByClientId?clientId=' + data.id
    return HttpReq.httpsRequest(url, data, 'GET');
}


export const getCostRecoreList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/queryExpenseCalendarByCardClientId/' + data.id
    return HttpReq.httpsRequest(url, '', 'GET');
}


export const getMakeCardUserList = async (data) => {
    const url = app.globalData.hostIp + '/weixin/retailer/GetStaff'
    return HttpReq.httpsRequest(url, data, 'GET');
}



export const subMakeCard = async (data) => {
    const url = app.globalData.hostIp + '/weixin/card/' + data.id
    return HttpReq.httpsRequest(url, '', 'GET');
}


export const subNewMakeCard = async (data) => {
    const url = app.globalData.hostIp + '/weixin/clientStore/queryIsNew'
    return HttpReq.httpsRequest(url, data, 'GET');
}

export const subNewMakeCard2 = async (data) => {
    const url = app.globalData.hostIp + '/weixin/store/' + data.storeId
    return HttpReq.httpsRequest(url, '', 'GET');
}


// export const addConsumerReq = async (data) => {
//     const url = app.globalData.hostIp + '/weixin/addConsumerReq'
//     return HttpReq.httpsRequest(url, data, 'POST');
// }

// 反馈

export const addClientFeedBackToRetailer = async (data) => {
    const url = app.globalData.hostIp + '/weixin/addClientFeedBackToRetailer'
    return HttpReq.httpsRequest(url, data, 'POST');
}

//扫码办卡
export const weixinBindClient= async (data) => {
  const url = app.globalData.hostIp + '/weixin/cardClient/bindClient'
  return HttpReq.httpsRequest(url, data, 'POST');
}





