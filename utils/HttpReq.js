
const app = getApp()
class HttpReq {
  static httpsRequest(url, data, method) {
    return new Promise((resolve, reject) => wx.request({
      url: url,
      data: data,
      header: {
        // token:"eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI5MTRhZjNkYTExYzA0MzQ0YTEyYTM0NmU2MGY5YWJlMSIsInN1YiI6IjIwIiwiaXNzIjoic2ciLCJpYXQiOjE2OTc5NzM3OTUsImV4cCI6MTY5ODA2MDE5NX0.D_epxuwpo9rEgU4DtPgOxTvDIUXmcJ3j5BOTXX0snvY"
      },
      method: method,
      success: function (res) { resolve(res.data) },
      fail: function (res) {
        wx.showToast({
          title: 'http网络错误',
          icon: 'none',
        });
        console.log(res)
      }
    }))
  }
}

module.exports = HttpReq